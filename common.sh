exe_path=$(readlink -e "${BASH_SOURCE[1]}")
exe_name=$(basename "$exe_path")
exe_dir=$(dirname "$exe_path")
exe_args=("$@")
log () { echo "$@" >&2; }
crash () { log "error: $@"; exit 1; }
set_disk_vars () {
    disk_id="$1"
    [[ "$disk_id" =~ ^[0-9][0-9][0-9]$ ]]
    disk_img="$exe_dir/disk-$disk_id.tar.asc"
    disk_dir="$exe_dir/disk-$disk_id"
}
trap 'crash "line $LINENO: exit code $?"' ERR
set -eEux
