FROM debian:stable
ARG DEBIAN_FRONTEND=noninteractive
ADD . /src/
WORKDIR /src
RUN ./setup
